import React from 'react';
import { Animated, AppRegistry, Image, Easing, View } from 'react-native';

export default class FadeInView extends React.Component {
  state = {
    fadeAnimation: new Animated.Value(0),  // Initial value for opacity: 0
  }

  duration = this.props.duration;
  delay = this.props.delay || 0;
  toValue = this.props.toValue || 1;

  // metodo chiamato appena avviene il caricamento del componente
  componentDidMount() {
    Animated.timing(                  // Animate over time
      this.state.fadeAnimation,            // The animated value to drive
      {
        toValue: this.toValue,                   // Animate to opacity: 1 (opaque)
        duration: this.duration,              // Make it take a while
        delay: this.delay,
        easing: Easing.bound(1)
      }
    ).start();                        // Starts the animation
  }

  render() {
    let { fadeAnimation } = this.state;

    return (
      <Animated.View                 // Special animatable View
        style={{
          ...this.props.style,
          opacity: fadeAnimation,         // Bind opacity to animated value
        }}
      >
        {this.props.children}
      </Animated.View>
    );
  }
}
