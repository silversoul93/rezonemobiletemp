import React from 'react';
import { Animated, Image, ImageBackground, TouchableOpacity, Text, View } from 'react-native';

import FadeInView from './src/fadeInView/js/FadeInView';

// You can then use your `FadeInView` in place of a `View` in your components:
export default class App extends React.Component {
  render() {
    return (
      <FadeInView duration={2000} style={{ width: "100%", height: "100%" }}>
        <ImageBackground style={{ width: "100%", height: "100%" }} source={require('./rezone_background.png')}>
          <FadeInView duration={4000} delay={3000} toValue={0.5} style={{ width: "100%", height: "100%" }}>
            <TouchableOpacity style={{ width: 50, height: 50, top: "91%", left: "80%" }} onPress={() => alert("Pizza")}>
              <Image style={{ width: 50, height: 50 }} source={require('./infos.png')} />
            </TouchableOpacity>
            <View style={{ flex: 1, flexDirection: "row", justifyContent: "center", alignContent: "center" }}>
              <TouchableOpacity style={{ width: "40%", height: "30%" }} onPress={() => alert("PizzaLogo")} >
                <Image source={require('./rezone_relatech_BW.png')} />
              </TouchableOpacity>
            </View>
          </FadeInView>
        </ImageBackground>
      </FadeInView>
    )
  }
 
}
